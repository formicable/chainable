![Alt text](logo.png?raw=true "PHP UNCHAINED")

chainable
=========

*Danger zone: never code this way*

Allows you to set a false value without breaking the chain. This is based on the odd behavior of php evaluating "0" strings as false. 

In this example, ```can('delete')``` will cause the entire chain to evaluate to false:

```php

class User
{
    use Chainable;

    private $permissions = ['read', 'write'];

    function can($permission)
    {
        if (!in_array($permission, $this->permissions)) {
            $this->returnFalse();
        }

        return $this;
    }


}

$user = new User;
echo ((string)$user->can('read')->can('write')) ? 'true' : 'false'; // true
echo ((string)$user->can('read')->can('delete')->can('write')) ? 'true' : 'false'; // false

```

Casting your evaluated method-chain to a string is how the magic happens - and another reason why you shouldn't use this.