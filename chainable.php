<?php

trait Chainable
{
    protected $return = true;

    function __toString()
    {
        return $this->return ? '1' : '0';
    }

    function returnFalse()
    {
        $this->return = false;
    }
}